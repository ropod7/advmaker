#!/usr/bin/env python
# -*- coding: utf-8 -*-

import jinja2, os, urllib

from bottle import route, run, request, redirect, abort, default_app
from pymongo import MongoClient

app = default_app()

template_env  = jinja2.Environment(loader=jinja2.FileSystemLoader(os.getcwd()))
template_url  = template_env.get_template('urls.html')
template_hits = template_env.get_template('hits.html')

client = MongoClient()
db     = client.advmaker
urls   = db.urls
hits   = db.hits

def execute_url():
    wrong_url = exists = False
    url = request.forms.get('url')
    try:
        urllib.urlopen(url)
    except IOError:
        wrong_url = True
    else:
        slug = url.replace('://', '-').replace('.', '-').replace('/', '-')
        url_exists = urls.find_one({'slug':slug})
        if url_exists:
            exists = url_exists.get('url')
        else:
            urls.insert({
                    'url'  : url,
                    'slug' : slug,
                    'hits': 0,
                        })

    return wrong_url, exists

# redirection to /urls URI
@route('/')
def redir():
    return redirect('/urls')

# working with urls
@route('/urls')
@route('/urls', method='POST')
def set_url():
    wrong_url = exists = False
    if 'url' in request.POST:
        wrong_url, exists = execute_url()

    context = {
            'urls'     : urls.find(),
            'exists'   : exists,
            'wrong_url': wrong_url,
                }

    return template_url.render(context)

# working with statistics
@route('/hits')
@route('/hits', method='POST')
def get_statistics():
    hits_all = obj = None
    if 'url' in request.POST:
        obj = request.forms.get('url')
        hits_all = hits.find({'object': urls.find_one({'slug':obj}).get('_id')})

    context = {
           'counter': urls.find_one({'slug':obj}).get('hits') if obj else None,
           'hits': hits_all,
           'urls': urls.find({'hits':{'$gte': 1}})    # shows urls in 'select field' with hits only
               }

    return template_hits.render(context)

# adding statistics to mongoDB
@route('/execute/<slug>')
def request_processing(slug):
    obj = urls.find_one({'slug':slug})
    if obj:
        urls.update({'slug': slug}, {'$inc': {'hits': 1}})
        hits.insert({
                'object'  : obj.get('_id'),
                'user_ip' : request.remote_addr
                    })
        return redirect(obj.get('url'))

    else:
        return abort(404, u'Извините, введён неверный параметр')

if __name__ == '__main__':
    run(host='localhost', port=8080, debug=True)