#!/usr/bin/env python
# -*- coding: utf-8 -*-

from unittest import TestCase, main

from webtest import TestApp

class TestApplication(TestCase):
    def setUp(self):
        import application

        self.application = application
        self.test_app = TestApp(application.app)

    def tearDown(self):
        self.application.client.drop_database('advmaker')

    def test_set_url_should_work_as_expected(self):
        """
        It should create new url properly.
        """
        target = 'http://www.google.com'
        response = self.test_app.post('/urls', params={'url': target})
        self.assertEqual(200, response.status_code)
        self.assertEqual(1, self.application.db.urls.count())
        self.assertEqual(target, self.application.db.urls.find_one().get('url'))
        return target

    def test_request_processing_works_as_expected(self):
        """
        It should record user click properly.
        """
        target = 'http://www.google.com'
        response = self.test_app.post('/urls', params={'url': target})
        url_slug = self.application.db.urls.find_one().get('slug')

        self.assertEqual(200, response.status_code)
        self.assertIsNotNone(url_slug)

        click_url = '/execute/{}'.format(url_slug)

        # Make 3 clicks.
        response = self.test_app.get(click_url)
        self.assertEqual(302, response.status_code)

        response = self.test_app.get(click_url)
        self.assertEqual(302, response.status_code)

        response = self.test_app.get(click_url)
        self.assertEqual(302, response.status_code)

        # Assert hits count.
        self.assertEqual(3, self.application.db.urls.find_one({'slug': url_slug}).get('hits'))

        # Assert click records.
        self.assertEqual(3, self.application.db.hits.count())

    def test_get_statistics_works_as_expected(self):
        """
        It should show statistics properly.
        """
        # It should create again new url properly:
        target = self.test_set_url_should_work_as_expected()

        url_slug = self.application.db.urls.find_one({'url':target}).get('slug')
        response = self.test_app.post('/hits', params={'url': url_slug})
        hits_all = self.application.db.hits.find()

        self.assertEqual(200, response.status_code)
        self.assertIsNotNone(hits_all)

        click_url = '/execute/{}'.format(url_slug)

        # Make 3 clicks.
        for i in range(3):
            response = self.test_app.get(click_url)
            self.assertEqual(302, response.status_code)

        # Assert hit objects count.
        self.assertEqual(3, self.application.db.hits.count())

# If tests are called from CLI, run directly.
if __name__ == '__main__':
    main()